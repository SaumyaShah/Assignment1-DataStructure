#include<stdio.h>
long long int dis[100002];
long long int vel[100002];
long long int arr[100002];


long long int merge(long long int start,long long int mid,long long int end)
{
	long long int i;
	long long int arr1[100002];
	long long int arr2[100002];
	long long int varr1[100002];
	long long int varr2[100002];

	for(i=0;i<mid-start;i++)
	{
		arr1[i]=dis[start+i];
		varr1[i]=vel[start+i];
	}

	for(i=0;i<end-mid;i++)
	{
		arr2[i]=dis[mid+i];
		varr2[i]=vel[mid+i];
	}

	long long int l=0;
	long long int r=0;
	long long int k=0;


	while(l<mid-start && r<end-mid)
	{
		if(arr1[l]<arr2[r])
		{
			dis[k+start]=arr1[l];
			vel[k+start]=varr1[l];
			l++;
			k++;
		}
		else if(arr1[l]>arr2[r])
		{
			dis[k+start]=arr2[r];
			vel[k+start]=varr2[r];
			r++;
			k++;
		}
		else
		{
			if(varr1[l]<varr2[r])
			{
				dis[k+start]=arr1[l];
				vel[k+start]=varr1[l];
				l++;
				k++;
			}
			else
			{
				dis[k+start]=arr2[r];
				vel[k+start]=varr2[r];
				r++;
				k++;

			}
		}
	}


	while(l<mid-start)
	{
		dis[k+start]=arr1[l];
		vel[k+start]=varr1[l];
		l++;
		k++;
	}
	while(r<end-mid)
	{
		arr[k+start]=arr2[r];
		vel[k+start]=varr2[r];
		r++;
		k++;

	}

/*	printf("arr:\n");

	for (long long int i = 0; i < n; ++i)
		{
			printf("%lld ",arr[i]);
			
		}	
		printf("\n");*/


	return 0;


}

long long int mergesort(long long int start,long long int end)
{
	if (end-start<=1)
	{
		return 0;
	}


	long long int mid;
	mid=(start+end)/2;

//	printf("fun called mergesort(%lld,%lld)\n",start,mid);
	mergesort(start,mid);
//	printf("fun called mergesort(%lld,%lld)\n",mid,end);
	mergesort(mid,end);

//	printf("fun called merge(%lld,%lld,%lld)\n",start,mid,end);
	merge(start,mid,end);

	return 0;
}





long long int invmerge(long long int l,long long int mid,long long int r)
{
	long long int count=0;
	long long int i=0,j=0,k=0,p;
	long long int left[100002];
	long long int right[100002];
	for(p=0;p<mid-l;p++)
		left[p]=arr[l+p];
	for(p=0;p<r-mid;p++)
		right[p]=arr[mid+p];
	while(i<mid-l && j<r-mid)
	{
		if(left[i]<=right[j])
		{
			arr[l+k]=left[i];
			i++;
			k++;
		}
		else
		{
              arr[l+k]=right[j];
              j++;
			  k++;
			  count=count+mid-l-i;
///			  printf("%lld ",count);
		}
	}
	while(i<mid-l)
	{
		arr[l+k]=left[i];
		k++;
		i++;
	}
	 while(j<r-mid)
        {
                  arr[k+l]=right[j];
                  k++;
                  j++;
        }
	 return count;
}


long long int invmergesort(long long int l,long long int r)
{
	//long long int count=0;
	//terminating condition
	if(r-l<=1)
		return 0;
	long long int mid=(l+r)/2;
	return invmergesort(l,mid) + invmergesort(mid,r) + invmerge(l,mid,r);
//	printf("%lld",count);0
//	return 0;
}




long long int check(long long int ctim,long long int k,long long int n)
{
	long long int tim=0;
	long long int count=0;
	long long int i;
	for (i = 0; i < n; i++)
	{
		arr[i]=dis[i]+vel[i]*ctim;
		/* code */
	}

	 long long int inv;
	 inv=invmergesort(0,n);

	if(inv>=k)
		return 1;
	else
		return 0;
}

long long int binsearch(long long int k,long long int n)
{
	long long int l=0;
	long long int r=10000000000;
	long long int mid;
	long long int ret;

	if(check(r+1,k,n)==0)
	{
		return -1;
	}

	while(r-l>=1)
	{
		mid=l+(r-l)/2;

		ret=check(mid,k,n);
//		printf("mid=%lld ret=%lld\n",mid,ret);

		if (ret==1)
		{
			r=mid;
			/* code */
		}
		else
		{
			l=mid+1;
		}

	}

//	printf("out of loop:r=%lld check(r,n,k)=%lld\n",r,check(r,k,n) );

	if (check(r,k,n)==1)
	{
		return r;
		/* code */
	}
	else
	{
		return r+1;
	}


}



int main()
{
	long long int n;
	long long int k;
//	long long int ctim;
	long long int i;

	scanf("%lld %lld",&n,&k);
//	scanf("%lld",&ctim);

//	long long inti;

	for(i=0;i<n;i++)
		scanf("%lld",&dis[i]);
	for(i=0;i<n;i++)
		scanf("%lld",&vel[i]);


	mergesort(0,n);

	/*printf("sorted array:\n");

	for (i= 0; i < n; ++i)
	{
		printf("%lld ",dis[i] );
	
	}
	printf("\n");

	for (i = 0; i < n; ++i)
	{
		printf("%lld ",vel[i] );
	}
	printf("\n");*/

	long long int ret=binsearch(k,n);

	printf("%lld\n",ret );

	return 0;
}