#include<stdio.h>

long long int arr[100002];
long long int lmax[100002];
long long int rmax[100002];
long long int lmin[100002];
long long int rmin[100002];
long long int M=1000000007;

long long int power(long long int a,long long int b)
{
	long long int p=1;
//	printf("in power fn a=%lld b=%lld\n",a,b );
	while(b>0)
	{
		if(b & 1==1)
		{
			p=((p%M)*(a%M))%M;
		}

		b=b >> 1;
		a=((a%M)*(a%M))%M;
	}

//	printf("in fun power p=%lld\n",p );

	return p;
}


int main()
{
	long long int n,k;
	long long int i;
	scanf("%lld %lld",&n,&k);
	for (i = 0; i < n; i++)
	{
		scanf("%lld",&arr[i]);
	}

	for (i = 0; i < 100002; i++)
	{
		lmin[i]=999999999999999;
		rmin[i]=999999999999999;
		/* code */
	}

	//lmax array
	i=0;
	long long int c=0;
	while(i<n)
	{	
		c=i%k;

		 if(c==0 && arr[i]>lmax[i])
		 	lmax[i]=arr[i];
		 else if(c!=0 && arr[i]>lmax[i-1])
		 	lmax[i]=arr[i];
		 else 
		 	lmax[i]=lmax[i-1];
		 i++;
	}

	//rmax array
	i=n-1;
	c=0;
	long long int rem=n%k;
	while(i>=0)
	{
		if (i>n-rem-1)
		{
			if(arr[i]>rmax[i])
				rmax[i]=arr[i];
			else
				rmax[i]=rmax[i+1];
			/* code */
		}
		else
		{
		
			if(c%k==0 && arr[i]>rmax[i])
				rmax[i]=arr[i];
			else if(c%k!=0 && arr[i]>rmax[i+1])
				rmax[i]=arr[i];
			else
				rmax[i]=rmax[i+1];
			c++;
		}
		i--;
	}
//	printf("lmax:\n");
/*	for (i = 0; i < n; i++)
	{
		printf("%lld ",lmax[i] );

	}
	printf("\n");
	printf("rmax:\n");
	for (i = 0; i < n; i++)
	{
		printf("%lld ",rmax[i] );

	}
	printf("\n");*/

	long long int max;
	long long int maxpro=1;
	for (i = k-1; i < n; i++)
	{
		if (lmax[i]>rmax[i-k+1])
		{	
			max=lmax[i];
			/* code */
		}
		else
		{
			max=rmax[i-k+1];
		}
		maxpro=((maxpro%M)*(max%M))%M;
//		printf("max=%lld\n",max );
		/* code */
	}


	//FOR MINIMUM

	//lmin array
	i=0;
	c=0;
	while(i<n)
	{	
		c=i%k;

		 if(c==0 && arr[i]<lmin[i])
		 	lmin[i]=arr[i];
		 else if(c!=0 && arr[i]<lmin[i-1])
		 	lmin[i]=arr[i];
		 else 
		 	lmin[i]=lmin[i-1];
		 i++;
	}

	//rmin array
	i=n-1;
	c=0;
	while(i>=0)
	{
		if (i>n-rem-1)
		{
			if(arr[i]<rmin[i])
				rmin[i]=arr[i];
			else
				rmin[i]=rmin[i+1];
			/* code */
		}
		else
		{
		
			if(c%k==0 && arr[i]<rmin[i])
				rmin[i]=arr[i];
			else if(c%k!=0 && arr[i]<rmin[i+1])
				rmin[i]=arr[i];
			else
				rmin[i]=rmin[i+1];
			c++;
		}
		i--;
	}
/*	printf("lmin:\n");
	for (i = 0; i < n; i++)
	{
		printf("%lld ",lmin[i] );
	}
	printf("\n");
	printf("rmin:\n");

	for (i = 0; i < n; i++)
	{
		printf("%lld ",rmin[i] );
		
	}
	printf("\n");*/

	long long int min;
	long long int minpro=1;
	for(i = k-1; i < n; i++)
	{
		if (lmin[i]<rmin[i-k+1])
		{	
			min=lmin[i];
			/* code */
		}
		else
		{
			min=rmin[i-k+1];
		}
		minpro=((minpro%M)*(min%M))%M;
//		printf("min=%lld\n",min );
		/* code */
	}

//	printf("maxpro=%lld\n",maxpro );
//	printf("minpro=%lld\n",minpro );

	long long int finalans=((maxpro%M)*(power(minpro,M-2))%M)%M;

//	long long int p=power(minpro,M-2)%M;
//	printf("p=%lld\n",p );

	printf("%lld\n",finalans);

	return 0;
}