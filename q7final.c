#include<stdio.h>
#include<string.h>
#include<stdlib.h>
long long int M=1000000007;

typedef struct Node
{
	long long int val;
	struct Node *prev;
	/* data */
}node;


node* push(long long int x,node *top)
{
//	printf("in push x=%lld\n",x );
	node *tmp=(node*)malloc(sizeof(node));

	tmp->val=x;
	tmp->prev=top;
	top=tmp;

	return top;

}

node* pop(node *top)
{
	node *temp=top;
	if(top->prev==NULL)
	{
		top=NULL;
	}
	else
	{
		top=top->prev;
	}

	free(temp);

	return top;
}


long long int power(long long int a,long long int b)
{
//	printf("in power a=%lld b=%lld\n",a,b);
	long long int p=1;
	while(b>0)
	{
//		printf("in while loop b=%lld\n", b);
		if (b & 1==1)
		{
			p=((p%M)*(a%M))%M;
		/* code */
		}
		b=b >> 1;
		a=((a%M)*(a%M))%M;
//		printf("at end while loop b=%lld\n", b);
	}
//	printf("p=%lld\n",p );
	return p;
}


int main()
{
	long long int t;
	scanf("%lld",&t);

	while(t--)
	{
		char str[200007];
		
		node* top=NULL;

	
		scanf("%s",str);
			/* code */
//		printf("str=%s\n",str);

		long long int len=strlen(str);

//		printf("strlen=%lld\n",len );

		long long int i;
		for ( i = 0; i < len; i++)
		{
//			printf("in for loop\n");
//			printf("above if str[i]=%c\n",str[i] );

			if(str[i]!='+' && str[i]!='-' && str[i]!='*' && str[i]!='/')
			{
				long long int  dig;
//				prlong longf("in if str[i]=%c\n",str[i] );
				dig=str[i]-'0';
				top=push(dig,top);

//				node *tmp=top;
/*				while(tmp!=NULL)
				{
//					prlong longf("%lld ",tmp->val );
					tmp=tmp->prev;
				}*/
//				printf("\n");
			}
			else
			{
				long long int temp2=top->val;

				top=pop(top);

				long long int  temp1=top->val;

				top=pop(top);

				long long int final;

				if(str[i]=='+')
				{
					final=((temp1%M)+(temp2%M))%M;

				}

				else if(str[i]=='-')
				{
//					printf("temp1=%lld temp2=%lld\n",temp1,temp2 );
					final=((temp1%M)-(temp2%M)+M)%M;
//					printf("final=%lld\n", final);

				}
				else if(str[i]=='*')
				{
					final=((temp1%M)*(temp2%M))%M;

				}
				else if(str[i]=='/')
				{
//					printf("temp1=%lld temp2=%lld\n",temp1,temp2 );
					final=((temp1%M)*(power(temp2,M-2))%M)%M;
				}

				top=push(final,top);
			}

			


			/* code */
		}

		long long int finalans=(top->val)%M;

		printf("%lld\n",finalans);


	}

	return 0;
}